const request = require("request-promise");

const schemaDefinition = `
type ServerInfo {
  """Current date"""
  currentDate: String!
  """Internal counter"""
  counter: Int!
}
type Query {
  """Number of seconds elapsed since 01.01.1970"""
  unixTime: Int
  """Internal server information"""
  serverInfo: ServerInfo
  """A list of random numbers"""
  randomNumbers(count: Int!, min: Int, max: Int): [Int]
}
type Mutation {
  """Increments counter variable by given amount"""
  increment(by: Int): ServerInfo
}
`;

let counter = 0;

const getRandomNumber = async (min, max) => {
  const rsp = await request({
    uri: `https://random-int.herokuapp.com/?min=${min}&max=${max}`,
    json: true,
  });
  return rsp.number;
};
const resolvers = {
  Query: {
    unixTime: () => Math.floor(Date.now() / 1000),
    serverInfo: () => ({}),
    randomNumbers: async (_, args = {}) => {
      const numbers = [];
      for (let i = 0; i < args.count; i++) {
        numbers.push(await getRandomNumber(args.min || 1, args.max || 1000));
      }
      return numbers;
    },
  },
  ServerInfo: {
    currentDate: () => new Date().toISOString(),
    counter: () => counter,
  },
  Mutation: {
    increment: (_, args = { by: 1 }) => {
      counter += args.by || 1;
      return counter;
    },
  },
};

module.exports = { schemaDefinition, resolvers };
